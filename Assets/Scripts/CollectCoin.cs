using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class CollectCoin : MonoBehaviour
{
    public UpdateCoinStats coinStats;
    private AudioSource collectCoinSound;

    // Start is called before the first frame update
    void Start()
    {
        collectCoinSound = GetComponent<AudioSource>();
    }

    // Collects coin
    void OnTriggerStay2D(Collider2D collision) 
    {
        coinStats.IncreaseCoins(20);
        collectCoinSound.Play();
        Object.Destroy(this.gameObject);
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class UpdateCoinStats : MonoBehaviour
{
    private Text thisText;
    private int coins = 0;
    // Start is called before the first frame update
    void Start()
    {
        thisText = GetComponent<Text>();
    }

    public void IncreaseCoins(int coinsToAdd) 
    {
        coins += coinsToAdd;
        thisText.text = coins + " coins";
    }

    // Update is called once per frame
    void Update()
    {
        
    }
}

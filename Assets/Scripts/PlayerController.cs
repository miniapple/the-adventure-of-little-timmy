using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerController : MonoBehaviour
{
    public float speed = 20.0f;
    public float horizontalInput;
    public Rigidbody2D rigidBody;
    public Vector2 jumpHeight = new Vector2(0,40.0f);
    public Animator playerAnimator;
    private BoxCollider2D footCollider;
    private bool isGrounded;
    public LayerMask ground;
    public float jumpWaitTime = 2.0f;
    private float jumpWaitTimer;
    public AudioSource music;
    public AudioSource jumpSound;

    // Start is called before the first frame update
    void Start()
    {
        rigidBody = GetComponent<Rigidbody2D>();
        footCollider = GetComponent<BoxCollider2D>();
    }

    // Update is called once per frame
    void Update()
    {
        // Moves player when left clicking
        horizontalInput = Input.GetAxis("Horizontal");
        isGrounded = footCollider.IsTouchingLayers(ground);
        float direction = Input.GetAxisRaw("Horizontal");
        rigidBody.velocity = new Vector2(speed * direction * Time.deltaTime, rigidBody.velocity.y);
        // If player is moving
        if (direction != 0f) {
            playerAnimator.SetBool("isRunning", true);
            transform.localScale = new Vector2(Mathf.Abs(transform.localScale.x) * direction, transform.localScale.y);
            //transform.Translate(new Vector3(horizontalInput, 0, 0) * speed * Time.deltaTime);
        } else {
            playerAnimator.SetBool("isRunning", false);
        }
        // Player jumps
        if (Input.GetKeyDown("space") || Input.GetKeyDown("up")) {
            if (isGrounded || jumpWaitTimer <= 0f) {
                jumpSound.Play();
                rigidBody.AddForce(jumpHeight, ForceMode2D.Impulse);
            }
        }
        // Player not on the ground
        if (!isGrounded) {
            playerAnimator.SetBool("isJumping", true);
            if (jumpWaitTimer > 0f) {
                jumpWaitTimer -= Time.deltaTime;
            }
        } else {
            playerAnimator.SetBool("isJumping", false);
            jumpWaitTimer = jumpWaitTime;
        }

        if (direction == 0f && isGrounded) {
            playerAnimator.SetBool("isIdle", true);
        } else {
            playerAnimator.SetBool("isIdle", false);
        }
    }
}
